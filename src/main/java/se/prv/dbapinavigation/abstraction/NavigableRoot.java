package se.prv.dbapinavigation.abstraction;

public interface NavigableRoot {
	
	NavigableTable[] getTables();
	void addConnectivityClass(String connectivityClass) throws ClassNotFoundException;
	void setPoolname(String poolname);
	
}
