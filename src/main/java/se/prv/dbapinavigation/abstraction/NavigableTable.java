package se.prv.dbapinavigation.abstraction;

public interface NavigableTable {

	String[] getReferredNames();
	NavigableTable getReferredTable(String referredName);
	
	String[] getReferringNames();
	NavigableTable getReferringTable(String referringName);
	
	String getName();
	Long getLength();
	NavigableObj[] getAll();
	NavigableObj newInstance();

	String[] getColumnNames();
	String getColumnType(String columnName);
	Integer getColumnMaxlength(String columnName);

	Boolean hasPrimary();
	Boolean columnIsBlob(String columnName);
	Boolean columnIsPrimary(String columnName);
	Boolean columnIsForeign(String columnName);
	String[] getColumnsForeign(String columnName);
	Boolean columnIsNullable(String columnName);

	SingleValueSearch[] getSingleSearches();
	SingleValueSearch getSingleSearcheOfColumn(String column);
	DoubleValueSearch[] getDoubleSearches();

}
