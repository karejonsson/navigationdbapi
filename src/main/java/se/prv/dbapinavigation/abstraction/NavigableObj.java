package se.prv.dbapinavigation.abstraction;

public interface NavigableObj {

	NavigableObj getReferredObj(String referredName);
	NavigableObj[] getReferringObjs(String referringName);
	
	Object getColumnValue(String columnName);	
	Boolean setColumnValue(String columnName, Object value) throws Exception;	
	
	byte[] getBlobContens(String blobColumnName);
	boolean setBlobContens(String blobColumnName, byte[] contents) throws Exception;
	
	boolean update() throws Exception;
	boolean insert() throws Exception;
	boolean delete() throws Exception;

	NavigableTable getTable();
	
}
