package se.prv.dbapinavigation.abstraction;

import java.util.List;

public interface SingleValueSearch {

	String getDescription();
	List<NavigableObj> singleSearch(String value, String operator) throws Exception;
	
}
