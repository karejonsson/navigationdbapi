package se.prv.dbapinavigation.abstraction;

import java.util.List;

public interface DoubleValueSearch {

	String getDescription();
	List<NavigableObj> doubleSearch(String value1, String operator1, String value2, String operator2);

}
