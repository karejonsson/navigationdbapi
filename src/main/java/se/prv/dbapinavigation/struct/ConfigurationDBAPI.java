package se.prv.dbapinavigation.struct;

import se.prv.dbapinavigation.abstraction.NavigableRoot;

public class ConfigurationDBAPI {
	
	private String name = null;

	private String connectivityclasses = null;
	private String rootclass = null;
	private String poolname = null;
	private String managementclass = null;
	private String configurationsymbol = null;
	private String configurationfile = null;
	
	public String toString() {
		return "ConfigurationDBAPI{name "+name+", connectivityclasses "+connectivityclasses+", rootclass "+rootclass+", poolname "+poolname+
				", managementclass "+managementclass+", configurationsymbol "+configurationsymbol+", configurationfile "+configurationfile+"}";
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getConnectivityclasses() {
		return connectivityclasses;
	}
	public void setConnectivityclasses(String connectivityclasses) {
		this.connectivityclasses = connectivityclasses;
	}
	public String getRootclass() {
		return rootclass;
	}
	public void setRootclass(String rootclass) {
		this.rootclass = rootclass;
	}
	public String getPoolname() {
		return poolname;
	}
	public void setPoolname(String poolname) {
		this.poolname = poolname;
	}
	public String getManagementclass() {
		return managementclass;
	}
	public void setManagementclass(String managementclass) {
		this.managementclass = managementclass;
	}
	public String getConfigurationsymbol() {
		return configurationsymbol;
	}
	public void setConfigurationsymbol(String configurationsymbol) {
		this.configurationsymbol = configurationsymbol;
	}
	public String getConfigurationfile() {
		return configurationfile;
	}
	public void setConfigurationfile(String configurationfile) {
		this.configurationfile = configurationfile;
	}
	
	private NavigableRoot nroot = null;
	private Boolean success = null;
	
	public NavigableRoot getRoot() {
		if(success != null) {
			if(success) {
				return nroot;
			}
			else {
				return null;
			}
		}
		if(nroot == null) {
			try {
				nroot = GetRoot.get(this);
				success = true;
				return nroot;
			} catch (Exception e) {
				e.printStackTrace();
			} 
		}
		success = false;
		return null;
	}
	
}
