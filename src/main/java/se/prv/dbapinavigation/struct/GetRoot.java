package se.prv.dbapinavigation.struct;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import general.reuse.properties.HarddriveProperties;
import se.prv.dbapinavigation.abstraction.NavigableRoot;

public class GetRoot {
	
	public static NavigableRoot get(ConfigurationDBAPI apiconfig) throws ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException, FileNotFoundException, IOException {
		Class managementclass = Class.forName(apiconfig.getManagementclass());
		Method method = managementclass.getMethod("addConnection", new Class[] { String.class, HarddriveProperties.class } );
		HarddriveProperties hp = new HarddriveProperties(apiconfig.getConfigurationsymbol(), apiconfig.getConfigurationfile());
		method.invoke(null, apiconfig.getPoolname(), hp);
		Class rootclass = Class.forName(apiconfig.getRootclass());
		NavigableRoot root = (NavigableRoot) rootclass.newInstance();
		String listfile = apiconfig.getConnectivityclasses();
		BufferedReader br = null;
		FileReader fr = null;
		try {
			fr = new FileReader(listfile);
			br = new BufferedReader(fr);
		    String line;
		    while ((line = br.readLine()) != null) {
		    	root.addConnectivityClass(line);
		    }
		}
		catch(Exception e) {
		}
		if(br != null) {
			try {
				br.close();
			}
			catch(Exception e) {}
		}
		if(fr != null) {
			try {
				fr.close();
			}
			catch(Exception e) {}
		}
		root.setPoolname(apiconfig.getPoolname());
		return root;
	}

}
