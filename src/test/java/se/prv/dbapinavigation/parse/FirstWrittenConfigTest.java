package se.prv.dbapinavigation.parse;

import java.io.InputStreamReader;
import java.util.List;

import org.junit.Test;

import junit.framework.Assert;
import se.prv.dbapinavigation.struct.ConfigurationDBAPI;

public class FirstWrittenConfigTest {

    @Test
    public void length() throws Exception {
        ClassLoader cl = DatabaseAPIConfigParser.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream("navigation1.config"));
        List<ConfigurationDBAPI> apis = DatabaseAPIConfigParser.parseConfig(isr);
        System.out.println("Antal "+apis.size());
        Assert.assertTrue(apis.size() == 4);
        for(ConfigurationDBAPI api : apis) {
        	System.out.println(api.toString());
        }
    }

    @Test
    public void t2() throws Exception {
        ClassLoader cl = DatabaseAPIConfigParser.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream("navigation2.config"));
        List<ConfigurationDBAPI> apis = DatabaseAPIConfigParser.parseConfig(isr);
        System.out.println("Antal "+apis.size());
        Assert.assertTrue(apis.size() == 20);
        for(ConfigurationDBAPI api : apis) {
        	System.out.println(api.toString());
        }
    }

}

